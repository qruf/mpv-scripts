A collection of mpv scripts.

## cropmode.lua

Adds two new key bindings: `crop-mode` toggles crop mode, and `crop-reset` removes the cropping. Entering crop mode binds `hkjl` to crop from the left, bottom, top, and right, `HJKL` to uncrop, and `s` to change the crop step size. These can be configured with script options. Leaving `crop-mode` removes the key bindings.

## fadeaf.lua

Adds two new script-messages: `afade` and `afade-remove`. `script-message afade 1 0.5 2` will fade the volume from 100% to 50% over two seconds. `script-message afade-remove` removes the filter. This is independent of mpv's regular volume control.

## filtercycle.lua

```
script-message-to filtercycle af crossfeed=strength=0.5 stereowiden=crossfeed=0.5 b2sb=feed=60
script-message-to filtercycle vf-pre format=colormatrix=bt.601 format=colormatrix=bt.709
```

Cycle between several filters. `vf` and `af` add the filter at the end of the chain, and `vf-pre` and `af-pre` add it at the front.
