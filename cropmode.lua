local fmt = string.format

local LABEL         = "crop_fb608e2f"
local LABEL_STEP    = LABEL .. "_step"
local REPEATABLE    = { repeatable = true }

local MODE_RESET    = -1
local MODE_INACTIVE = 0
local MODE_ACTIVE   = 1

local active = MODE_RESET
local step   = 1
local vsize  = { w = 0, h = 0 }
local sides  = { l = 0, b = 0, t = 0, r = 0 }
local binds  = {}

local opts = {
    crop_left     = "h",
    crop_bottom   = "j",
    crop_top      = "k",
    crop_right    = "l",
    uncrop_left   = "H",
    uncrop_bottom = "J",
    uncrop_top    = "K",
    uncrop_right  = "L",
    step_size     = "s"
}
require("mp.options").read_options(opts, "cropmode")

local remove = function()
    mp.command(fmt("no-osd vf remove @%s", LABEL))
end

local set_crop = function()
    local w = vsize.w - sides.l - sides.r
    local h = vsize.h - sides.t - sides.b
    remove()
    -- the lavfi=[] form correctly displays the crop params in the osd
    mp.command(fmt("vf add @%s:lavfi=[crop=%d:%d:%d:%d]", LABEL, w, h, sides.l, sides.t))
end

local invalid_crop = function(side)
    if side == "l" or side == "r" then
        return vsize.w < sides.l + sides.r + (2 ^ step)
    end
    return vsize.h < sides.t + sides.b + (2 ^ step)
end

for k, v in pairs(opts) do
    local which, side = string.match(k, "()crop_([lbtr])")
    if which == 1 then
        binds[v] = function()
            if invalid_crop(side) then
                return
            end
            sides[side] = sides[side] + (2 ^ step)
            return set_crop()
        end
    elseif which == 3 then
        binds[v] = function() 
            sides[side] = math.max(sides[side] - (2 ^ step), 0) 
            return set_crop() 
        end
    end
end

local onload = function()
    vsize.w = mp.get_property_native("width")
    vsize.h = mp.get_property_native("height")
end

local step_size = function()
    step = (step % 5) + 1
    mp.osd_message(fmt("crop step size: %d", 2 ^ step))
end

local mode_enter = function()
    active = MODE_ACTIVE
    for k, v in pairs(binds) do
        mp.add_forced_key_binding(k, fmt("%s_%s", LABEL, k), v, REPEATABLE)
    end
    mp.add_forced_key_binding(opts.step_size, LABEL_STEP, step_size)
    mp.osd_message("crop mode active")
end

local mode_exit = function()
    active = MODE_INACTIVE
    for k, v in pairs(binds) do
        mp.remove_key_binding(fmt("%s_%s", LABEL, k))
    end
    mp.remove_key_binding(LABEL_STEP)
    mp.osd_message("crop mode inactive")
end

local mode_toggle = function() 
    if active == MODE_ACTIVE then
        return mode_exit() 
    end
    return mode_enter()
end

local mode_reset = function()
    if active == MODE_RESET then
        return
    end
    mode_exit()
    active = MODE_RESET
    remove()
    mp.osd_message("crop mode reset")
    sides.l, sides.b, sides.t, sides.r = 0, 0, 0, 0
end

mp.register_event("file-loaded", onload)
mp.add_key_binding(nil, "crop-mode", mode_toggle)
mp.add_key_binding(nil, "crop-reset", mode_reset)
