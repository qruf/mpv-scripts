-- script-message-to filtercycle af crossfeed=strength=0.5 stereowiden=crossfeed=0.5 b2sb=feed=60

local select, fmt = select, string.format
local cache, label = {}, 1

local function init(t, i, ...)
    if i == 0 then
        t[1], t[2] = label, 0
        return t, true
    end
    local out = {}
    t[...] = out
    return init(out, i - 1, select(2, ...))
end

local function fetch(t, i, ...)
    if i == 0 then
        return t[1] and t or init(t, 0)
    end
    local out = t[...]
    if not out then
        return init(t, i, ...)
    end
    return fetch(out, i - 1, select(2, ...))
end

local toggle = function(av, name, i)
    return i > 0 and mp.command(fmt("%s toggle @fcyc_%02d_%02d", av, name, i))
end

local cycle = function(av, func, ...)
    local count = select("#", ...)
    local chain, new = fetch(cache, count, ...)
    local name, active = chain[1], chain[2]
    if new then
        label = label + 1
        for i = 1, count do
            mp.command(fmt("%s %s @fcyc_%02d_%02d:!%s", av, func, name, i, select(i, ...)))
        end
    end
    toggle(av, name, active)
    active = (active + 1) % (count + 1)
    toggle(av, name, active)
    chain[2] = active
end

local register = function(name, av, func)
    local f = function(...)
        return cycle(av, func, ...)
    end
    mp.register_script_message(name, f)
end

register("af", "af", "append")
register("vf", "vf", "append")
register("af-pre", "af", "pre")
register("vf-pre", "vf", "pre")
