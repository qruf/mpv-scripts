-- fade the volume from 100% to 60% over two seconds:
-- script-message afade 1 0.6 2

local fmt = string.format
local label = "afade_756d3f6c"

local valid = function(n)
    return n and n >= 0 and n <= 1
end

local remove = function()
    mp.command(fmt("af remove @%s", label))
end

local fade = function(_start, _target, _dur)
    local start, target, dur = tonumber(_start), tonumber(_target), tonumber(_dur)
    if not (valid(start) and valid(target) and dur) then
        return mp.msg.error("invalid arguments to afade:", _start, _target, _dur)
    end
    remove()
    local which, silence, unity = "out", target, start
    if start < target then
        which, silence, unity = "in", start, target
    end
    local ts = mp.get_property_native("time-pos/full")
    mp.command(fmt("af add @%s:afade=t=%s:st=%f:d=%f:silence=%f:unity=%f", label, which, ts, dur, silence, unity))
end

mp.register_script_message("afade", fade) 
mp.register_script_message("afade-remove", remove) 
